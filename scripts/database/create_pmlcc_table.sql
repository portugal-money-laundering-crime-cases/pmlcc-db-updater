create table pmlcc (
    id uuid primary key,
    crime_number int8,
    year_date VARCHAR (10),
    update_date VARCHAR (15)
);