#!/bin/bash

FILE_PATH="$(dirname "$(realpath $0)")"

host="--host=127.0.0.1"
port="-p 5432"
user="-U postgres"
password=""
password_command="-w"

while getopts h:p:u:w:H flag
do
    case "${flag}" in
        h) host="-h $OPTARG";;
        p) port="-p $OPTARG";;
        u) user="-U $OPTARG";;
        w) password=${OPTARG};;
        H) help="help";;
        \?) echo "Invalid option: -$OPTARG. Use -H flag for help."
            exit
    esac
done

if [[ $help == "help" ]]
then
    echo "Options:
    -h HOST         Peak number of concurrent Locust users
    -p PORT         Rate to spawn users
    -u USER         Stop after the specified amount of time (300s, 20m, 1h, 1h30m)
    -w PASSWORD     Python module to import
    -H              Script file help"
else

    if [[ $password != "" ]]
    then
        export PGPASSWORD=${password}
        echo $PGPASSWORD
        password_command=""
    fi

    psql $host $port $user $password_command -f $FILE_PATH/create_database.sql

    psql $host $port $user $password_command -d pmlcc_db -f $FILE_PATH/create_pmlcc_table.sql
fi